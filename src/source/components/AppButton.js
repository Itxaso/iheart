import React, {Component} from 'react';
import { Button } from 'react-native-elements';

export default class AppButton extends Component{
    render(){
        const {action, iconName, iconColor, title, bgColor, width} = this.props;
        return(
            <Button
                onPress={action}
                buttonStyle={{
                    backgroundColor:bgColor,
                    height:45,
                    borderColor:"transparent",
                    borderWidth:0,
                    borderRadius:5,
                    marginBottom:5,
                    marginLeft:5,
                    marginRight:5,
                    width: width,
                    alignItems: 'center'
                }}
                title={title}
               /* icon={
                    <Icon
                        name={iconName}
                        size={15}
                        color={iconColor}
                    />
                }*/
                text={title}
                iconRight={true}
            >
            </Button>
        );
    }
}