import React, {Component} from 'react';
import { ImageBackground } from 'react-native';
import PropTypes from 'prop-types';

export default class BackgroundImage extends Component{
    render(){
        const {source, children, customStyles} = this.props;
        return(
            <ImageBackground
                source={source}
                style={[{flex:1, width:null, height:null}, customStyles]}
            >
                {children}
            </ImageBackground>
        );
    }
}

BackgroundImage.propTypes = {
  customStyles: PropTypes.shape({}),
};