import React, {Component} from 'react';
import SplashScreen from 'react-native-splash-screen';
import GuestNavigation from './navigation/guest';
import firebaseConfig from './utils/firebase';
import * as firebase from 'firebase';
firebase.initializeApp(firebaseConfig);



const user = 0;

export default class App extends Component {

  constructor () {
    super();
        this.state = {
            isLogged: false,
            loaded: false
        }
  }

  async componentDidMount(){
    SplashScreen.hide();

    await firebase.auth().onAuthStateChanged((user) => {
          if(user !== null) {
              this.setState({
                  isLogged: true,
                  loaded: true
              });
          } else {
              this.setState({
                  isLogged: false,
                  loaded: true
              });
          }
    })
  }

  render() {
      const {isLogged, loaded} = this.state;

      return(
        <GuestNavigation/>
      );
  }
}



