import React from 'react';
import {StackNavigator} from "react-navigation";
import StartScreen from "../screens/Start";
import LoginScreen from "../screens/Login";
import HeartRateScreen from "../screens/HeartRate";
import RegisterScreen from "../screens/Register";

export default StackNavigator(
    {
        Start:{
            screen: StartScreen
        },
        Login:{
            screen: LoginScreen
        },
        HeartRate:{
            screen: HeartRateScreen
        },
        Register:{
            screen: RegisterScreen
        }
    },
    {
        initialRouteName: 'Start',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#EC5870'
            },
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf: 'center',
                fontSize: 20,
                color: '#fff',
                fontWeight: 'bold',
                alignItems: 'center'
            }
        }
    }
)