import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import BackgroundImage from "../components/BackgroundImage";
import { Icon } from 'react-native-elements'

export default class Start extends Component {
    render(){
        return(
            <BackgroundImage source={this.props.navigation.state.params.link}>
                <View style={styles.container}>
                    <Text style={styles.rate}>{this.props.navigation.state.params.heartRateNumber} BPM</Text>
                    <Icon
                        raised
                        name='heartbeat'
                        type='font-awesome'
                        size={80}
                        color='#f50'/>
                    <Text style={styles.rateText}>{this.props.navigation.state.params.text}</Text>
                </View>
            </BackgroundImage>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rate:{
        fontSize: 35,
        fontWeight:'bold',
        textAlign: 'center',
        margin: 40,
        color: '#fff',
    },
    rateText:{
        fontSize: 20,
        textAlign: 'center',
        margin: 40,
        color: '#fff',
    }
});