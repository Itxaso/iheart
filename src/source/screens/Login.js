import React, {Component} from 'react';
import {Dimensions, View} from 'react-native';
import BackgroundImage from "../components/BackgroundImage";
import AppButton from "../components/AppButton";
import t from 'tcomb-form-native';
import FormValidation from '../../source/utils/validation';
import {Card} from "react-native-elements";
const Form = t.form.Form;
import * as firebase from "firebase";
import Toast from 'react-native-simple-toast';

export default class Login extends Component{
    constructor (){
        super();

        this.user = t.struct({
            email:FormValidation.email,
            password:FormValidation.password,
        });

        this.options = {
            fields: {
                email:{
                    help: 'Enter your email',
                    error: 'Incorrect email',
                    autoCapitalize: 'none',
                },
                password:{
                    help:'Enter your password',
                    error:'Incorrect password',
                    password: true,
                    secureTextEntry: true,
                }
            }
        }
    }

    login(){
        const validate = this.refs.form.getValue();
        if(validate){
            firebase.auth().signInWithEmailAndPassword(validate.email, validate.password)
                .then(()=>{
                    Toast.showWithGravity("Bienvenido", Toast.LONG, Toast.BOTTOM);
                })
                .catch((error) => {
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    if(errorCode==='auth/wrong-password'){
                        Toast.showWithGravity('Password incorrecto', Toast.LONG, Toast.BOTTOM)
                    }else{
                        Toast.showWithGravity(errorMessage, Toast.LONG, Toast.BOTTOM);
                    }
                });
        }
    }

    render(){
        const width = 200;
        return(
            <BackgroundImage source={require('../../assets/images/fondo.png')}>
                <View>
                    <Card wrapperStyle={{paddingLeft:10}} title="Login">
                        <Form
                            ref="form"
                            type={this.user}
                            options={this.options}
                        />
                        <AppButton
                            bgColor="rgba(111,38,74,0.7)"
                            title="Login"
                            action={this.login.bind(this)}
                            iconColor="#fff"
                            width={width}
                        />
                    </Card>
                </View>
            </BackgroundImage>
        );
    }
}