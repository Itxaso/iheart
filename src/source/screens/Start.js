import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import BackgroundImage from '../components/BackgroundImage';
import AppButton from '../components/AppButton';
import { BleManager } from 'react-native-ble-plx';
import { NavigationActions } from 'react-navigation';
import {Dimensions} from 'react-native';
import * as firebase from 'firebase';

export default class Start extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(){
        super();
        this.manager = new BleManager();
        this.state = {
            devices: ''
        };
    }

    componentWillMount() {
        const subscription = this.manager.onStateChange((state) => {
            if (state === 'PoweredOn') {
                this.scanAndConnect();
                subscription.remove();
            }else{
                console.log('el bluetooth esta apagado: ', state);
            }
        }, true);
    }

    scanAndConnect() {
        //Simulator heartRate code
        const RandomNumber = Math.floor(Math.random() * (190-60)) + 60;

        var assets = require('../../assets/images/HeartRateRelax.png');
        var textInfo = 'Keep this relax!';
        if(RandomNumber < 81){
            assets = require('../../assets/images/HeartRateRelax.png');
            textInfo = 'Keep this relax!';
        }else if(RandomNumber < 101){
            assets = require('../../assets/images/NiceMood.png');
            textInfo = 'Weah, what a nice mood!';
        }else if(RandomNumber < 150){
            assets = require('../../assets/images/Stress.png');
            textInfo = 'It seems you are stressed';
        }else if(RandomNumber < 191){
            assets = require('../../assets/images/Alert.png');
            textInfo = 'Call a doctor!';
        }
        //End simulator

        const navigateAction = NavigationActions.navigate({
            routeName: 'HeartRate',
            params: { heartRateNumber: RandomNumber, link: assets, text:textInfo}
        });

        this.props.navigation.dispatch(navigateAction);
        this.manager.startDeviceScan(null, null, (error, device) => {
            if (error) {
                // Handle error (scanning will be stopped automatically)
                return
            }

            // Check if it is a device you are looking for based on advertisement data
            // or other criteria.
            if (device) {
                console.log('there is a device: ', device);
                this.setState({ devices: device.name });
                // Stop scanning as it's not necessary if you are scanning for one device.
                this.manager.stopDeviceScan();

                // Proceed with connection.
            }
        });
    }

    login(){
        const navigateAction = NavigationActions.navigate({
            routeName: 'Login'
        });
        this.props.navigation.dispatch(navigateAction);
    }

    register(){
        const navigateAction = NavigationActions.navigate({
            routeName: 'Register'
        });
        this.props.navigation.dispatch(navigateAction);
    }

    facebook(){

    }

    render(){
        const {width} = Dimensions.get('window');
        return(
            <BackgroundImage source={require('../../assets/images/fondo-heart.png')}>
                <View style={styles.container}>
                    <Text style={styles.welcome}>Welcome to iHeart App!</Text>
                    <AppButton
                        bgColor="rgba(111,38,74,0.7)"
                        title="Scan and Connect"
                        action={this.scanAndConnect.bind(this)}
                        iconName="sign-in"
                        iconSize={30}
                        iconColor="#fff"
                        width={width}
                    />
                    <AppButton
                        bgColor = "rgba(200,200,50, 0.7)"
                        title="Login"
                        action={this.login.bind(this)}
                        iconName="user-plus"
                        iconSize={30}
                        iconColor="#fff"
                        width={width}
                    />
                    <AppButton
                        bgColor = "rgba(67,67,146,0.7)"
                        title="Register"
                        action={this.register.bind(this)}
                        iconName="facebook"
                        iconSize={30}
                        iconColor="#fff"
                        width={width}
                    />
                </View>
            </BackgroundImage>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 40,
        color: '#fff',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});